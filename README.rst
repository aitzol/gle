gle (Gabonetako Lagun Ezkutua)
==============================

Gabonetako lagun ezkutuaren zozketa modu teknofiloago batean egiteko saiakera bat baino ez da hau.

Sarreran CSV fitxategi bi zutabetako fitxategi bat hartzen du; lehen zutabean izena eta bigarren 
zutabean partaide horri dagokion email helbidea. Sarrerako datu horiek nahasi eta aleatorioki bikoteak
egiten ditu. Horrela nahi baldin bada emaitza emailez bidali daiteke, edo bestela pantailan inprimatu

Instalazioa 
------------
- Python ingurune virtual bat erabiltzea gomendatzen da
- Klonatu repositorioa
- pip install gle

Erabilera
---------

Behin instalatuta hau exekutatu behar da:

.. code-block::shell

Usage: mixlist [OPTIONS] CSV_FILE_PATH

  CSV_FILE_PATH fitxategiko partehartzaileak hartu eta nahastutako zerrenda
  bat bueltatu. Fitxategiak bi zutabe izan beharko ditu partehartzailea,
  partehartzailearen_emaila formatuan.

Options:
  --send BOOLEAN         Emaitza emailez bidali
  --email_template TEXT  Email bidalketarako txantiloi fitxategia.  Python
                        format-ekin erabiltzeko nork eta nori aldagaiak
                        erabili daitezke

  --help                 Show this message and exit.

Email bidalketa
---------------
Email bidalketarako `Sendgrid <https://sendgrid.com>`_ API_KEY bat beharrezkoa da.
API_KEYa eskuratu ondoren ingurune aldagai bidez esportatu. Adibidez:

.. code-block::shell

export SENDGRID_API_KEY=API_KEY

export from_email=Bidaltzailearen emaila
