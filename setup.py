from setuptools import setup, find_packages
import sys, os

version = "0.0"

setup(
    name="gle",
    version=version,
    description="Gabonetako lagun ezkutua antolatzeko",
    long_description="""\
""",
    classifiers=[],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords="",
    author="Aitzol Naberan",
    author_email="anaberan@gmail.com",
    url="",
    license="GPL",
    packages=find_packages(exclude=["ez_setup", "examples", "tests"]),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "sendgrid",
        "Click",
        "python-dotenv==0.15.0"
        # -*- Extra requirements: -*-
    ],
    entry_points={"console_scripts": ["mixlist = gle.pairs:main",],},
)
