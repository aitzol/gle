import click
import csv
import os
import random
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail


def printlist(mixed):
    for k, v in mixed.items():
        print("{who}\t->\t{whom}\n".format(who=k, whom=v))


def sendlist(mixed, template=None):
    for data in mixed.values():
        nork = data["nork"]["izena"]
        to_email = data["nork"]["email"]
        nori = data["nori"]
        message = Mail(
            from_email=os.environ.get("from_email"),
            to_emails=to_email,
            subject="Lagun ezkutua",
            html_content=template.format(nork=nork, nori=nori),
        )
        import pdb

        pdb.set_trace()
        sg = SendGridAPIClient(os.environ.get("SENDGRID_API_KEY"))
        response = sg.send(message)
        print(response)


def pairs_from_list(elegible):
    pairs = {}
    random.shuffle(elegible)
    pairs[elegible[-1][0]] = {
        "nori": elegible[0][0],
        "nork": {"email": elegible[-1][1], "izena": elegible[-1][0]},
    }
    for index in range(0, len(elegible) - 1):
        pairs[elegible[index][0]] = {
            "nori": elegible[index + 1][0],
            "nork": {"izena": elegible[index][0], "email": elegible[index][1]},
        }
    return pairs


@click.command()
@click.option("--send", default=False, help="Emaitza emailez bidali", type=click.BOOL)
@click.option(
    "--email_template",
    default=None,
    help="""Email bidalketarako txantiloi fitxategia. 
    Python format-ekin erabiltzeko nork eta nori aldagaiak erabili daitezke""",
)
@click.argument("csv_file_path")
def main(send, email_template, csv_file_path):
    """
    CSV_FILE_PATH fitxategiko partehartzaileak hartu eta nahastutako zerrenda bat bueltatu. Fitxategiak
    bi zutabe izan beharko ditu partehartzailea, partehartzailearen_emaila formatuan.

    
    """
    with open(csv_file_path, "r") as partehartzaileak_file:
        reader = csv.reader(partehartzaileak_file)
        mixed = pairs_from_list([partehartzailea for partehartzailea in reader])
        printlist(mixed)
        if send:
            if email_template is not None:
                with open(email_template, "r") as template_file:
                    email_template = template_file.read()
            else:
                email_template = """Kaixo {nork}.
                Lagun ezkutua {nori}-ri egin behar diozu
                """
            sendlist(mixed, email_template)


if __name__ == "__main__":
    test_data = ["U1", "U2", "U3"]
    printlist(pairs_from_list(test_data))
