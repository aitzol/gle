import random
import argparse

def printlist(mixed):
    for k, v in mixed.items():
        print("{who}\t->\t{whom}\n".format(who=k,whom=v))
    
def pairs_from_list(elegible):
    pairs = {}
    random.shuffle(elegible)
    pairs[elegible[-1]] = elegible[0]
    for index in range(0, len(elegible) - 1):
        pairs[elegible[index]] = elegible[index + 1]
    return pairs

def main():
    parser = argparse.ArgumentParser(
        description='Lagun ezkutuak egiteko')
    parser.add_argument('partehartzaileak',
                        type=str,
                        help='Parte hartuko dutenen zerrenda'
                        )   
    arguments = parser.parse_args()
    original = arguments.partehartzaileak.split(',')
    printlist(pairs_from_list(original))
        

if __name__ == '__main__':
    test_data =[ 'U1', 'U2', 'U3' ]
    printlist(pairs_from_list(test_data))
        
